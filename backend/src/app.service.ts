import { Injectable } from '@nestjs/common';
import { AppModel } from './app.model';

@Injectable()
export class AppService {
  constructor(private readonly model: AppModel) { }
  createUrl(url: string): string {
    return this.model.createUrl(url);
  }
  getUrl(id: string): any {
    return this.model.getUrl(id);
  }
  getDomains(): string[] {
    return this.model.getDomains();
  }
}
