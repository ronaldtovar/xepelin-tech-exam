import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppModel } from './app.model';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, AppModel],
})
export class AppModule { }
