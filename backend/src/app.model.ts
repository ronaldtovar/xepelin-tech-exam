import { Injectable, ParseUUIDPipe } from '@nestjs/common';
import { v4 as uuid } from "uuid";
import { LocalStorage } from "node-localstorage";

@Injectable()
export class AppModel {
    DATA_STORAGE = "urls";
    localStorage;
    constructor() {
        this.localStorage = new LocalStorage('./urls');
    }
    createUrl(url: string): string {
        //get domain TODO
        const domain = this.getDomainFromUrl(url);
        const records = this.getRecords();
        const current = this.searchByDomain(domain, records);
        if (current) {
            return current.id;
        }
        //save record
        const id = uuid();
        const record ={ id, url: domain };
        this.saveNewRecord(record, records);
        
        //return the id
        return id;
    }

    private saveNewRecord(record: any, records){
        records.push(record);
        this.localStorage.setItem(this.DATA_STORAGE, JSON.stringify(records))
    }

    getUrl(id: string): any {
        return this.getRecords().find(r => r.id == id);
    }

    getDomains(): string[] {
        const records = this.getRecords().map(r =>{
            return r.url;
        });

        return records;
    }

    searchByDomain(domain: string, records: any[]) {
        return records.find(r => r.url == domain);
    }

    private getRecords(): any[] {
        return JSON.parse(this.localStorage.getItem(this.DATA_STORAGE) || "[]");
    }

    private getDomainFromUrl(url: string): string {
        const domain = new URL(url);
        return domain.host;
    }
}
