import { Controller, Get, Post, Body, Req, Res, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Post("shorten")
  createUrl(@Res() res, @Body() body: any) {
    try{
      const response = this.appService.createUrl(body.url);
      res.status(201).json(response);
    }
    catch(err){
      console.log(err);
      res.status(500).json("There was an error");
    }
  }

  @Get("domain")
  getDomain(@Res() res) {
    try{
      const response = this.appService.getDomains() || [];
      res.status(200).json(response);
    }
    catch(err){
      console.log(err);
      res.status(500).json("There was an error");
    }
  }
  
  @Get("/:id")
  getUrl(@Res() res, @Param() param: any) {
    try{
      const response =this.appService.getUrl(param.id);
      if(!response){
        res.status(404).json("Url not found")
      }
      res.status(200).json(response);
    }
    catch(err){
      console.log(err);
      res.status(500).json("There was an error");
    }
  }


}
